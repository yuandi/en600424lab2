from playground import playgroundlog

from playground.twisted.endpoints import GateClientEndpoint
from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler

import RIPClientLayer

from twisted.internet import stdio
from twisted.internet import reactor
from twisted.internet.protocol import Protocol, Factory, connectionDone
from twisted.protocols import basic

import sys, time, os, logging
from twisted.internet.endpoints import connectProtocol
from playground.network.common.Timer import callLater

class ClientProtocol(Protocol):
	def __init__(self, callback):
		self.buffer = ""
		self.callback = callback

	def close(self):
		send.__sendMessageActual("__QUIT__")

	def connectionMade(self):
		print "ClientConnection Made"

	def dataReceived(self, data):
		self.buffer += data
		self.callback(data)

		self.buffer and self.dataReceived('')

	def send(self, data):
		self.transport.write(data)

class ClientFactory(Factory):
	protocol=ClientProtocol

class ClientTest(basic.LineReceiver):

	delimiter = os.linesep

	def __init__(self, ServerAddr, endpoint):
		self.__ServerAddr = ServerAddr
		self.__protocol = None
		self.__endpoint = endpoint
		self.__d = None

	def __handleError(self, e):
		print "had a failure", e
		raise Exception("Failure: " + str(e))

	def __handleMsg(self, msg):
		print"\nReceived message from server: %s" % msg
		self.reset()

	def connectionLost(self, reason = connectionDone):
		callLater(1.0, reactor.stop)

	def connectionMade(self):
		self.__protocol = ClientProtocol(self.__handleMsg)
		self.__d = connectProtocol(self.__endpoint, self.__protocol)
		self.__d.addCallback(self.RIPConnectionMade)
		self.__d.addErrback(self.__handleError)

	def RIPConnectionMade(self, status):
		self.transport.write("Message to send to %s (quit to exit): " %self.__ServerAddr)

	def lineReceived(self, line):
		if not self.__protocol:
			self.transport.write("protocol not yet ready.\n")
		message = line
		if message.lower().strip() in ["quit", "__quit__"]:
			self.__protocol.transport.loseConnection()
			self.__protocol = None
			self.exit("Normal Exit")
			return
		else:
			self.__protocol.send(message)
			self.reset()

	def reset(self):
		self.transport.write("\nMessage to send to %s (quit to exit): "% self.__ServerAddr)

	def exit(self, reason = None):
		print "Shutdown of echo test client. Reason = ", reason
		if self.transport:
			self.transport.loseConnection()

if __name__ == "__main__":
	mode = "20164.0.0.1"
	gatekey = "gatekey1"
	
	logctx = playgroundlog.LoggingContext("RIP_"+str(mode))

	playgroundlog.startLogging(logctx)
	playgroundlog.UseStdErrHandler(True)

	ServerAddr = mode

	ClientEndpoint = GateClientEndpoint.CreateFromConfig(reactor, ServerAddr, 101, gatekey, networkStack = RIPClientLayer)
	tester = ClientTest(ServerAddr, ClientEndpoint)

	stdio.StandardIO(tester)
	
	TwistedShutdownErrorHandler.HandleRootFatalErrors()
	reactor.run()

