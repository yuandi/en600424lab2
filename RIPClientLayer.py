from twisted.internet.protocol import Protocol, Factory
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, UINT4, BOOL1, OPTIONAL, DEFAULT_VALUE, LIST
from playground.network.common.Protocol import StackingTransport, StackingProtocolMixin, StackingFactoryMixin, MessageStorage
from playground.twisted.endpoints import GateClientEndpoint
from playground.network.common.statemachine.StateMachine import StateMachine
from twisted.internet import reactor
import random

from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
#import CertFactory

class RIPMessage(MessageDefinition):
	PLAYGROUND_IDENTIFIER = "RIP.RIPMessage"
	MESSAGE_VERSION = "1.0"

	BODY = [("sequence_number", UINT4),
		("acknowledgement_number", UINT4, OPTIONAL),
		("signature", STRING, DEFAULT_VALUE("")),
		("certificate", LIST(STRING), OPTIONAL),
		("sessionID", STRING),
		("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
		("close_flag", BOOL1, DEFAULT_VALUE(False)),
		("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
		("reset_flag", BOOL1, DEFAULT_VALUE(False)),
		("data", STRING,DEFAULT_VALUE("")),
		("OPTIONS", LIST(STRING), OPTIONAL)
		]


class RIPTransport(StackingTransport):
	def __init__(self, lowerTransport):
		StackingTransport.__init__(self, lowerTransport)

	def write(self, data):
		Rip_Message = RIPMessage()
		Rip_Message.sequence_number = 001
		Rip_Message.acknowledgement_number = 002
		Rip_Message.signature = "this is signature field."
		Rip_Message.certificate = "this is cer field"
		Rip_Message.sessionID = "this is session ID, 224"
		Rip_Message.acknowledgement_flag = False
		Rip_Message.close_flag = True
		Rip_Message.sequence_number_notification_flag = False
		Rip_Message.reset_flag = False
		Rip_Message.data = data
		Rip_Message.OPTIONS = "this is OPTIONS"
		self.lowerTransport().write(Rip_Message.__serialize__())


class RIPProtocol(StackingProtocolMixin, Protocol):
	def __init__(self):
		self.buffer = ""
		self.__RIPMsgVctr = MessageStorage(RIPMessage)
		self.fsm = StateMachine("RIPClientStateMachine")
#		self.fsm.addState("CLOSED", ("SENDSNN", "SNN-SENT"))
		self.fsm.addState("SNN-SENT", ("SNN_ACK-RCV", "ESTABLISHED"), onExit = self.SendFinalACK)
		self.localsqnum = 0

	def SendFinalACK(self, signal, RcvedMsg):
		ThirdHandshakePack = RIPMessage()
		ThirdHandshakePack.sequence_number = RcvedMsg.acknowledgement_number
		ThirdHandshakePack.acknowledgement_number = RcvedMsg.sequence_number + 1
		ThirdHandshakePack.acknowledgement_flag = True
		ThirdHandshakePack.sessionID = "this is sessionID field"
		self.transport.write(ThirdHandshakePack.__serialize__())
		print "phase 3\n"
		higherTransport = RIPTransport(self.transport)
		self.makeHigherConnection(higherTransport)

	def connectionMade(self):
		FirstHandshakePack = RIPMessage()
		self.localsqnum = random.randint(0, 0xffffffff)
		FirstHandshakePack.sequence_number = self.localsqnum
		FirstHandshakePack.sequence_number_notification_flag = True
		FirstHandshakePack.sessionID = self.localsqnum
		FirstHandshakePack.certificate = "cert field"
		FirstHandshakePack.certificate.append(self.localsqnum)
		self.transport.write(FirstHandshakePack.__serialize__())		
		self.fsm.start("SNN-SENT")
		print "phase 1\n"
#		higherTransport = RIPTransport(self.transport)
#		self.makeHigherConnection(higherTransport)

	def dataReceived(self, data):
#		self.buffer += data
#		Rip_Message, bytesUsed = RIPMessage.Deserialize(data)
#		self.buffer = self.buffer[bytesUsed:]
#		self.higherProtocol() and self.higherProtocol().dataReceived(data)
#		self.buffer and self.dataReceived("")
		self.__RIPMsgVctr.update(data)
		RIPMsg = self.__RIPMsgVctr.popMessage()
#		self.higherProtocol() and self.higherProtocol().dataReceived(RIPMsg.data)
		print "inside>>]????"
		if (RIPMsg.sequence_number_notification_flag == True and RIPMsg.acknowledgement_flag == True):
			self.fsm.signal("SNN_ACK-RCV", RIPMsg)
		else:
			if not (RIPMsg.sequence_number_notification_flag == True):
				print "snnflag fail"
			if not (RIPMsg.acknowledgement_flag == True):
				print "ackflag fail"
			print "not got into if"

class RIPCFactory(StackingFactoryMixin, Factory):
	def buildProtocol(self, addr):
		return RIPProtocol()

ConnectFactory = RIPCFactory
#ListenFactory = RIPFactory
