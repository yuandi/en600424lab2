from playground.network.message.ProtoBuilder import MessageDefinition
import RIPServerLayer
from RIPServerLayer import RIPMessage

from playground import playgroundlog
from playground.network.common import PlaygroundAddress
from playground.network.message.StandardMessageSpecifiers import STRING, UINT4, BOOL1, OPTIONAL, DEFAULT_VALUE, LIST

from playground.twisted.endpoints import GateServerEndpoint
from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler
from playground.network.common.Protocol import StackingTransport, StackingProtocolMixin, StackingFactoryMixin, MessageStorage

from twisted.internet import reactor
from twisted.internet.protocol import Protocol, Factory, connectionDone
from twisted.protocols import basic

import logging
from twisted.internet.endpoints import connectProtocol
logger = logging.getLogger(__name__)

#class RIPMessage(MessageDefinition):
#	PLAYGROUND_IDENTIFIER = "RIP.RIPMessage"
#	MESSAGE_VERSION = "1.0"
#
#	BODY = [("sequence_number", UINT4),
#		("acknowledgement_number", UINT4, OPTIONAL),
#		("signature", STRING, DEFAULT_VALUE("")),
#		("certificate", LIST(STRING), OPTIONAL),
#		("sessionID", STRING),
#		("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
#		("close_flag", BOOL1, DEFAULT_VALUE(False)),
#		("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
#		("reset_flag", BOOL1, DEFAULT_VALUE(False)),
#		("data", STRING,DEFAULT_VALUE("")),
#		("OPTIONS", LIST(STRING), OPTIONAL)
#		]

class ServerProtocol(Protocol):

	def __init__(self):
		self.buffer = ""
		self.__RIPMsgVctr = MessageStorage(RIPMessage)

	def connectionLost(self, reason = connectionDone):
		print "Lost connection to client. Cleaning up."
		Protocol.connectionLost(self, reason = reason)

	def dataReceived(self, data):
		self.buffer += data
		RcvedMessage, bytesUsed = RIPMessage.Deserialize(data)
		self.buffer = self.buffer[bytesUsed:]
		self.buffer += data
		print data

class RIPMessageServerFactory(Factory):
	protocol = ServerProtocol

if __name__ == "__main__":
	gateKey = "gatekey1"
	mode = "server"
	
	logctx = playgroundlog.LoggingContext("server_"+str(mode))
	playgroundlog.startLogging(logctx)
	playgroundlog.UseStdErrHandler(True)

	Server = RIPMessageServerFactory()

	ServerEndpoint = GateServerEndpoint.CreateFromConfig(reactor, 101, gateKey, networkStack = RIPServerLayer)
	d = ServerEndpoint.listen(Server)
	d.addErrback(logger.error)

	TwistedShutdownErrorHandler.HandleRootFatalErrors()
	reactor.run()
