from twisted.internet.protocol import Protocol, Factory

from playground.error import GetErrorReporter

from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, UINT4, BOOL1, OPTIONAL, DEFAULT_VALUE, LIST
from playground.network.common.Protocol import StackingTransport, StackingProtocolMixin, StackingFactoryMixin, MessageStorage
from playground.network.common.Packet import Packet, PacketStorage, IterateMessages
from playground.twisted.endpoints import GateClientEndpoint
from playground.network.common.statemachine.StateMachine import StateMachine
from twisted.internet import reactor
import logging, random

from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
#import CertFactory

logger = logging.getLogger(__name__)
errReporter = GetErrorReporter(__name__)

class RIPMessage(MessageDefinition):
	PLAYGROUND_IDENTIFIER = "RIP.RIPMessage"
	MESSAGE_VERSION = "1.0"

	BODY = [("sequence_number", UINT4),
		("acknowledgement_number", UINT4, OPTIONAL),
		("signature", STRING, DEFAULT_VALUE("")),
		("certificate", LIST(STRING), OPTIONAL),
		("sessionID", STRING),
		("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
		("close_flag", BOOL1, DEFAULT_VALUE(False)),
		("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
		("reset_flag", BOOL1, DEFAULT_VALUE(False)),
		("data", STRING,DEFAULT_VALUE("")),
		("OPTIONS", LIST(STRING), OPTIONAL)
		]


class RIPTransport(StackingTransport):
	def __init__(self, lowerTransport):
		StackingTransport.__init__(self, lowerTransport)

	def write(self, data):
		Rip_Message = RIPMessage()
		Rip_Message.sequence_number = 001
		Rip_Message.acknowledgement_number = 002
		Rip_Message.signature = "this is signature field."
		Rip_Message.certificate = "this is cer field"
		Rip_Message.sessionID = "this is session ID, 224"
		Rip_Message.acknowledgement_flag = False
		Rip_Message.close_flag = True
		Rip_Message.sequence_number_notification_flag = False
		Rip_Message.reset_flag = False
		Rip_Message.data = data
		Rip_Message.OPTIONS = "this is OPTIONS"
		self.lowerTransport().write(Rip_Message.__serialize__())




class RIPProtocol(StackingProtocolMixin, Protocol):

	STATE_LISTEN  = "LISTEN"
	SIGNAL_RCV_SNN = "RCV_SNN"
	STATE_SNN_RCV = "SNN-RECV"

	def __init__(self):
		self.buffer = ""
		self.__RIPMsgVctr = PacketStorage()
		self.localsqnum = 0
		self.estblshFlag = False
		self.__fsm = StateMachine("RIPServerStateMachine")
		self.__fsm.addState(self.STATE_LISTEN, (self.SIGNAL_RCV_SNN, self.STATE_SNN_RCV), onExit = self.SendSNNACK)
		self.__fsm.addState("SNN-RECV", ("SNN_ACK_RCV", "ESTABLISHED"), onExit = self.CompleteHandshake)
		self.__fsm.addState("ESTABLISHED", ("uselesssignal", "ignorthis"))
		self.__fsm.start("LISTEN")

	def SendSNNACK(self, RcvedMsg):
		print "\nentered SNN function\n"
		Package = RIPMessage()
		localsqnum = random.randint(0, 0xffffffff)
		Package.sequence_number = self.localsqnum
		Package.acknowledgement_flag = True
		ClientCert = RcvedMsg.certificate.pop()
		nonce1 = RcvedMsg.certificate.pop()
		
		Package.acknowledgement_number = RcvedMsg.sequence_number + 1
		Package.sequence_number_notification_flag = True
		Package.sessionID = "this is session ID field"
		self.transport.write(Package.__serialize__())
		print "phase 2\n"

	def CompleteHandshake(self, RcvedMsg):
		print "\n entered ack function\n"
		self.estblshFlag = True
		self.localsqnum = RcvedMsg.acknowledgement_number
		higherTransport = RIPTransport(self.transport)
		self.makeHigherConnection(higherTransport)
		print "phase 3\n"	

	def connectionMade(self):
                print "111"
#		higherTransport = RIPTransport(self.transport)
#		self.setHigherProtocol(HigherProtocol)
#		self.makeHigherConnection(higherTransport)
		self.fsm.start("LISTEN")
		print "start listening\n"



	def dataReceived(self, data):
#		self.buffer += data
#		Rip_Message, bytesUsed = RIPMessage.Deserialize(data)
#		self.buffer = self.buffer[bytesUsed:]
#		self.higherProtocol() and self.higherProtocol().dataReceived(data)
#		self.buffer and self.dataReceived("")

		self.buffer += data
		msg, bytesUsed = RIPMessage.Deserialize(data)
		self.buffer = self.buffer[bytesUsed:]
		
		if msg.sequence_number_notification_flag == True:
			print "\nenter SNN\n"
			self.__fsm.signal(self.SIGNAL_RCV_SNN, msg)
			self.SendSNNACK(msg)
			print "\n SNN signal finished\n"
		if (msg.acknowledgement_flag == True and self.estblshFlag == False):
			print "\nenter ack\n"
			self.__fsm.signal("SNN_ACK_RCV", "123")
			self.CompleteHandshake(msg)

#		self.__RIPMsgVctr.update(data)
#		for msg in IterateMessages(self.__RIPMsgVctr):
#			print "\nenterloop\n"
#			if msg.sequence_number_notification_flag == True:
#				print "\nenter SNN\n"
#				self.fsm.signal("RCV_ANN", msg)
#			if (msg.acknowledgement_flag == True and self.estblshFlag == False):
#				print "\nenter ack\n"
#				self.fsm.signal("SNN_ACK_RCV", msg)

		self.__RIPMsgVctr.update(data)
		RIPMsg = self.__RIPMsgVctr.popMessage()
		if RIPMsg.sequence_number_notification_flag == True:
			print "\nthis is SNN\n"
			self.fsm.signal("RCV_SNN", RIPMsg)
		if (RIPMsg.acknowledgement_flag == True and self.estblshFlag == False):
			print "\nthis is ack\n"
			self.fsm.signal("SNN_ACK_RCV", RIPMsg)
#		self.higherProtocol() and self.higherProtocol().dataReceived(RIPMsg.data)





class RIPFactory(StackingFactoryMixin, Factory):
	def buildProtocol(self, addr):
		return RIPProtocol()

#ConnectFactory = RIPFactory
ListenFactory = RIPFactory
